﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.OleDb;

namespace _460Lab4
{
    /// <summary>
    /// Interaction logic for NewStudentPage.xaml
    /// </summary>
    public partial class NewStudentPage : Page
    {
        public NewStudentPage()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            //create connection
            String basePath = Environment.CurrentDirectory;
            basePath += @"\..\..";   //FOR DEBUGGING, remove if deploying
            OleDbConnection conn = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " +
                basePath + @"\Data\Database.mdb;" + "User Id=Admin;Password=;");
            conn.Open();

            //insert
            OleDbCommand insertCommand = new OleDbCommand("insert into Students (FirstName, LastName, Department, DateOfBirth, Grade) values (?, ?, ?, ?, ?)", conn);
            insertCommand.Parameters.AddWithValue("FirstName", txtFirstName.Text);
            insertCommand.Parameters.AddWithValue("LastName", txtLastName.Text);
            insertCommand.Parameters.AddWithValue("Department", txtDepartment.Text);
            insertCommand.Parameters.AddWithValue("DateOfBirth", txtBirthdate.Text);
            insertCommand.Parameters.AddWithValue("Grade", txtGrade.Text);
            insertCommand.ExecuteNonQuery();
            conn.Close();

            this.NavigationService.Navigate(new StudentsPage());

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new StudentsPage());
        }
    }
}
