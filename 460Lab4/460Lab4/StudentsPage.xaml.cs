﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.OleDb;

namespace _460Lab4
{
    /// <summary>
    /// Interaction logic for StudentsPage.xaml
    /// </summary>
    public partial class StudentsPage : Page
    {
        public StudentsPage()
        {
            InitializeComponent();

            //create connection
            String basePath = Environment.CurrentDirectory;
            basePath += @"\..\..";   //FOR DEBUGGING, remove if deploying
            OleDbConnection conn = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " +
                basePath + @"\Data\Database.mdb;" + "User Id=Admin;Password=;");
            conn.Open();

            //retrive data
            OleDbCommand command = new OleDbCommand("select * from Students", conn);

            //set datagrid source
            dg.ItemsSource = command.ExecuteReader();
        }

        private void btnNewStudent_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new NewStudentPage());
        }
    }
}
